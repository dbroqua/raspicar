# raspicar

Get and Display car sensors

## Requirements

### Hardware

- Raspberry Pi Zero or other Raspberry Pi version (3 or 4)
- [Optional] UBEC DC/DC Step-Down (Buck) Converter - 5V @ 3A output
- Adafruit 0.56" 4-Digit 7-Segment Display w/I2C Backpack (or Adafruit Quad Alphanumeric Display - 0.54" Digits w/ I2C Backpack)
- Voltage sensor (tested with Grove - Voltage Divider and ARCELI DC0-25V)
- Temperature sensor (TMP36)
- MCP3008 - 8-Channel 10-Bit ADC With SPI Interface
- Momentary switch button

### Software

- Raspios
- pip3 (sudo apt install python3-pip)
- HT16K33 Library (sudo pip3 install adafruit-circuitpython-ht16k33)
- Pillow Library (sudo apt install python3-pil)

## Wiring

![Default Wiring](./medias/RaspiCar.png)
