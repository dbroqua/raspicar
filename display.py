import board
from adafruit_ht16k33.segments import Seg7x4
from adafruit_ht16k33.segments import Seg14x4


class Display:
    """Class for communicate with display"""

    def __init__(self, config):
        i2c = board.I2C()
        model = config.get('display', 'MODEL')

        self.display = None

        if (model == '7x4'):
            self.display = Seg7x4(i2c)
        elif (model == '14x4'):
            self.display = Seg14x4(i2c)
        else:
            print('==== ERROR: Unknown display model ====')
            return -1

        self.display.brightness = float(config.get('display', 'BRIGHTNESS'))
        self.display.blink_rate = int(config.get('display', 'BLINK_RATE'))

    def clear(self):
        self.display.fill(0)

    def print(self, value):
        self.clear()
        self.display.print(str(value))
