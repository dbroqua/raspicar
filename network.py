import RPi.GPIO as GPIO
from urllib import request


class Network:
    """Class to check network connectivity and display result"""

    def __init__(self, config):
        self.ledpin = int(config.get('network', 'LED'))
        GPIO.setup(self.ledpin, GPIO.OUT)
        GPIO.output(self.ledpin, GPIO.LOW)
        self.host = config.get('network', 'URL')

    def check(self):
        try:
            request.urlopen(self.host)
            GPIO.output(self.ledpin,  GPIO.HIGH)
            return True
        except:
            GPIO.output(self.ledpin, GPIO.LOW)
            return False
